#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int lireligne(int fd, char *buffer, int size) {
    ssize_t nbread = read(fd, buffer, size);
    if (nbread == -1) {
        return -1;
    }

    int i;
    for (i = 0; i < nbread; i++) {
        if (buffer[i] == '\n') {
            i++;
            break; // correction: ajout du point-virgule manquant
        }
    }
    lseek(fd, i - nbread, SEEK_CUR);
    return i;
}

void inversion(int fd_in, int fd_out, unsigned char *buffer) {
    int sz;
    for (int i = 0; i < 3; i++) {
        sz = lireligne(fd_in, buffer, 100);
        write(fd_out, buffer, sz);
    }
    while (sz >= 1) {
        sz = read(fd_in, buffer, 1);
        buffer[0] = 255 - buffer[0];
        write(fd_out, buffer, sz);
    }
}

int main(int argc, char *argv[]) {
    int fd_in;   // descripteur de fichier du fichier ouvert en lecture
    int fd_out;  // descripteur de fichier du fichier ouvert en écriture
    int nbread;
    int sz;
    unsigned char *buffer = malloc(4096 * sizeof(unsigned char));    // buffer de lecture
    fd_in = open(argv[1], O_RDONLY);
    if (fd_in == -1) {
        perror("Erreur lors de l'ouverture du fichier en lecture");
        exit(EXIT_FAILURE);
    }
    fd_out = open("copy.pgm", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fd_out == -1) {
        perror("Erreur lors de l'ouverture du fichier en écriture");
        exit(EXIT_FAILURE);
    }
    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Erreur lors de la création du processus fils");
        exit(EXIT_FAILURE);
    }
    if (child_pid == 0) {
        printf("Je suis le processus fils avec PID: %d\n", getpid());

        inversion(fd_in, fd_out, buffer);
    }
    else {
        printf("Je suis le processus parent avec PID: %d\n", getpid());
        inversion(fd_in, fd_out, buffer);
    }
    close(fd_in);
    close(fd_out);
    free(buffer);
    return 0;
}
