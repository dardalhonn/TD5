#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv) {
    unsigned int sleep(unsigned int seconde);
    int i;
    sleep(5);
    printf("PID: %d (avant fork)\n", getpid());
    i = fork();
    sleep(5);
    if (i != 0) {
        printf("PID: %d, résultat du fork: %d\n",getpid(),i);

    } else {
        sleep(5);
        printf("PID: %d, résultat du fork: %d \n",getpid(),i);
    }
    sleep(5);
    printf("PID: %d (après fork)\n", getpid());
}
